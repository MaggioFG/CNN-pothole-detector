# CNN Pothole Classifier

CNN implementation both one dimensional (without preprocessing the data) either two dimensional, using the Wavelet transform.
In the first case, the achieved accuracy is about the 88%, proving to be a good model. In the second case, several waveforms of the Wavelet function
were applied, and the results showed that the best model, i.e. the one with the highest accuracy in the detection of the potholes is the one trained with the
Frequency B-Spline Wavelet (fbsp) as input, which reaches a score equal to 93%.

## Project structure

The project has been organized in different modules, in particular:
### Dataset_creator.py:

Thanks to the functions present in this module, we can take the csv files created
during the acquisitions of the smart boxes and the smartphones and manipulate
them. In particular, we can cut in the surroundings of an event and resample the
data as we want. After applying the desired transformations to the dataset, the
new one can be stored as several csv file representing the individual events.

### Neural_networks.py:

This file contains the definitions of all the models used throughout the project.
Furthermore, a Factory function has been written to simplify the creation of new
parametrized objects. This module will be described extensively in a following
paragraph.

### Accelerometer_classifier.py:

The classifier module contains all the classes and functions needed to handle the
training and testing of the machine learning models. This goes from loading the
dataset in memory, pre-process it according to the features that one wants to
extract, to initialize the model, train it, and finally testing it.

### Image_classifier.py:

The image classifier basically does the same operation as the accelerometer one.
But being the dataset completely different in type, this other module to handle the
specific needs of images has been created, for the sake of simplicity.

### Data_visualization.py:

An important step for every machine learning task is to properly visualize the data
in order to get useful insight on how to optimize the training of the models. Indeed,
this module provides several visualization tools, to look at the samples of the
datasets and to visualize metrics of the models, like the loss values on the test
and training set as the epochs of training pass.