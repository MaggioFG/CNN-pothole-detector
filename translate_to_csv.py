import re
import datetime
import pytz
import os

if not os.path.exists("dataset/translated_sure_pothole"):
    os.makedirs("dataset/translated_sure_pothole")
if not os.path.exists("dataset/translated_sure_smooth"):
    os.makedirs("dataset/translated_sure_smooth")


def translate(fileinput, fileoutput):
    # Pattern for GPS data
    pattern_gps = r"^P\d*\s*([\d\/]+)\s*([\d:]+)\s*([+\d.]+)\s*([+\d.]+)[\s\d.+-e]*(\*\*)*"

    # Pattern for High Detailed data
    pattern_hd = r"^H\d*\s*([\d.]+)\s*([\+\-\d.]+)\s*([\+\-\d.]+)\s*([\+\-\d.]+)\s*(\*\*)*"

    # Pattern for Low Detailed data
    pattern_ld = r"^L\d*\s*([\d.]+)\s*([\+\-\d.]+)\s*([\+\-\d.]+)\s*([\+\-\d.]+)\s*(\*\*)*"

    # Pattern for Speed
    pattern_speed = r".*\s*SPD:(\d*)"

    gps_data = {}
    hd_data = []
    ld_data = []
    speed = 0
    crash_timestamp = 0

    with open(fileinput, 'r') as f:
        line = f.readline()

        while line:

            rspeed = re.search(pattern_speed, line)
            if rspeed:
                speed = rspeed.group(1)
                print("Detected speed of the event is: {}".format(speed))

            rgps = re.search(pattern_gps, line)
            if rgps:
                date, hour = rgps.group(1), rgps.group(2)
                lat, lon = rgps.group(3), rgps.group(4)
                gps_crash_label = bool(rgps.group(5))
                if gps_crash_label:
                    crash_time_str = f"{date} {hour}"
                    crash_timestamp = datetime.datetime.strptime(crash_time_str, '%Y/%m/%d %H:%M:%S')
                    print(f"Crash time found in file: {crash_time_str}")
                    _utc = pytz.timezone("UTC")
                    _local = pytz.timezone("Europe/Paris")
                    crash_timestamp = _utc.localize(crash_timestamp, is_dst=None)
                    crash_timestamp = crash_timestamp.astimezone(_local)
                    print(f"Time is in UTC. Converting to local is: {crash_timestamp}")
                    crash_timestamp = int(crash_timestamp.timestamp() * 1000)
                    print(f"Converted to timestamp is: {crash_timestamp}")
                # print(f"New GPS data found: {date} {hour} {lat} {lon}")
                key = date.replace("/", "") + hour.replace(":", "")
                gps_data[key] = (lat, lon, gps_crash_label)

            rhd = re.search(pattern_hd, line)
            if rhd:
                offset = rhd.group(1)
                x = rhd.group(2)
                y = rhd.group(3)
                z = rhd.group(4)
                crash_label = bool(rhd.group(5))
                # print(f"New H data found: {offset}, {x}, {y}, {z}, {crash_label}")
                hd_data.append([offset, x, y, z, crash_label])

            lhd = re.search(pattern_ld, line)
            if lhd:
                offset = lhd.group(1)
                x = lhd.group(2)
                y = lhd.group(3)
                z = lhd.group(4)
                crash_label = bool(lhd.group(5))
                # print(f"New L data found: {offset}, {x}, {y}, {z}, {crash_label}")
                if crash_label:
                    ld_data.append([crash_timestamp, offset, x, y, z, crash_label])
                else:
                    ld_data.append(["None", offset, x, y, z, crash_label])

            line = f.readline()

    # Quick fix to a problem in the logger that shows wrong digits on timestamp
    for index, element in enumerate(ld_data):
        element[1] = "{0:.3f}".format(round(float(element[1]), 3) - index * 0.03)

    print(f"{len(ld_data)} samples found")
    print(gps_data)
    print(hd_data)
    print(ld_data)

    with open(fileoutput, 'w') as f:
        print("WRITING")
        f.write(f"SPEED:{speed}\n")
        for row in ld_data:
            f.write(f"{row[0]},{row[1].replace('.', '')},{row[2]},{row[3]},{row[4]},0,0,{row[5]}\n")


if __name__ == "__main__":
    translate("dataset/sure_pothole/CrashInfo_5UTM_01_2019-10-24_16-59-57.log",
              "DELETE_ME.csv")
