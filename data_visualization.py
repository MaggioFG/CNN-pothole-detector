import pandas as pd
from dataset_creator import load_accelerometer_data
import matplotlib.pyplot as plt
import os
##
FILE_PATH = "../smartphone_dataset/window-1500-frequency-50__Only_LG/Master_label/smooth/LG-H970-2019-10-26 09:52:47.100.csv"

# BOX Pothole
FILE_PATH = "resampled_dataset/to_graph/window-1500-frequency-100/1/SmartbBox-2019-10-26 09:45:47.990.csv"
# Same pothole with smartphone
FILE_PATH = "resampled_dataset/to_graph/window-1500-frequency-100/1/LG-H970-2019-10-26 09:45:43.900.csv"

# Smooth
FILE_PATH = "resampled_dataset/to_graph/window-1500-frequency-100/smooth/SmartbBox-2019-10-27 16:12:56.990.csv"
# Same smooth, Phone NOT UNDERSAMPLED
FILE_PATH = "resampled_dataset/manual_notunder/manual_smooth-LG-H970-2019-10-27 16:13:06.280.csv"
# Same smooth, Phone at 50Hz
FILE_PATH = "resampled_dataset/to_graph/50Hz/LG-H970-2019-10-27 16:13:06.280.csv"
# FILE_PATH = "resampled_dataset/to_graph/window-1500-frequency-100/1/LG-H970-2019-10-27 16:13:06.280.csv"

FILE_NAME = os.path.basename(FILE_PATH)
# MODEL_LOG = "model_logs/Hybrid_CNN_2d/Hybrid_CNN_2d_C32_C64_L32_L64_relu _100Batch_1000Epochs_1574011770.log"
# MODEL_LOG = "model_logs/window3_with_mac_new_axes/Hybrid_CNN_2d/Hybrid_CNN_2d_C32_C64_L32_L64_relu _HE_initialized_100Batch_1000Epochs_1574067727.log"
# MODEL_LOG = "model_logs/to_graph/Hybrid_CNN_2d/Hybrid_CNN_2d_C32_C64_L32_L64_reluActivation_0.5Dropout _HE_initialized_44Batch_2000Epochs_0.01LR__1574083519.log"
# MODEL_LOG = "model_logs/to_graph_last/CNN_2d/CNN_2d_C32_C64_L32_L64_reluActivation_0.3Dropout _HE_initialized_44Batch_1000Epochs_0.005LR_0.0005WD__1574419582.log"
# MODEL_LOG = "model_logs/learning_rate/CNN_2d/CNN_2d_5KernelSize_C32_C64_L32_L64_reluActivation_0.3Dropout _HE_initialized_44Batch_800Epochs_0.001LR_0.0005WD_0.9Momentum_SGDOptimizer1574620859.log"
# MODEL_LOG = "model_logs/learning_rate/CNN_2d/CNN_2d_5KernelSize_C32_C64_L32_L64_reluActivation_0.3Dropout _HE_initialized_44Batch_800Epochs_0.001LR_0.0005WD_0.9Momentum_AdamAmsGradOptimizer1574622009.log"
# MODEL_LOG = "model_logs/learning_rate/CNN_2d/CNN_2d_5KernelSize_C32_C64_L32_L64_reluActivation_0.3Dropout _HE_initialized_88Batch_600Epochs_0.001LR_0.0005WD_0.9Momentum_AdamAmsGradOptimizer1574623009.log"
MODEL_LOG = "model_logs/batch_norm/CNN_2D/CNN_2D_5KernelSize_C32_C64_L256_L128_reluActivation_0.3Dropout_BatchNorm_HE_initialized_88Batch_400Epochs_0.001LR_0.0005WD_0.9Momentum_SGDOptimizer1574683233.log"
# MODEL_LOG = "model_logs/batch_norm/CNN_2D/CNN_2D_5KernelSize_C32_C64_L256_L128_reluActivation_0.3Dropout_BatchNorm_HE_initialized_88Batch_400Epochs_0.001LR_0.0005WD_0.9Momentum_AdamOptimizer1574695205.log"
# MODEL_LOG = "model_logs/batch_norm/CNN_2D/CNN_2D_5KernelSize_C32_C64_L256_L128_reluActivation_0.3Dropout_BatchNorm_HE_initialized_88Batch_800Epochs_0.0005LR_0.0005WD_0.9Momentum_AdamOptimizer1574695871.log"
# MODEL_LOG = "model_logs/batch_norm/CNN_2D/CNN_2D_5KernelSize_C16_C32_C64_L256_L128_reluActivation_0.3Dropout_BatchNorm_HE_initialized_88Batch_400Epochs_0.001LR_0.0005WD_0.9Momentum_AdamOptimizer1574696658.log"
MODEL_LOG = "model_logs/batch_norm/CNN_1D/CNN_1D_5KernelSize_C32_C64_L256_L128_reluActivation_0.5Dropout_BatchNorm_HE_initialized_88Batch_500Epochs_0.001LR_0.0005WD_0.9Momentum_AdamOptimizer1574706941.log"

# CNN 1D, 88%
MODEL_LOG = "/home/mario/Desktop/Mario/Studio+/Magis/fmdks/project/Link to pytoncinio/pytorchinio/model_logs/batch_norm/CNN_1D/CNN_1D_5KernelSize_C32_C64_L256_L128_reluActivation_0.5Dropout_BatchNorm_HE_initialized_88Batch_128Epochs_0.001LR_0.001WD_0.9Momentum_AdamOptimizer1574709962.log"
# CNN 1D, 90%
MODEL_LOG = "/home/mario/Desktop/Mario/Studio+/Magis/fmdks/project/Link to pytoncinio/pytorchinio/model_logs/batch_norm/CNN_1D/CNN_1D_5KernelSize_C32_C64_L256_L128_reluActivation_0.5Dropout_BatchNorm_HE_initialized_88Batch_150Epochs_0.001LR_0.0005WD_0.9Momentum_AdamOptimizer1574703775.log"
#CNN 2D, 83%
# MODEL_LOG = "/home/mario/Desktop/Mario/Studio+/Magis/fmdks/project/Link to pytoncinio/pytorchinio/model_logs/batch_norm/CNN_2D/CNN_2D_5KernelSize_C32_C64_L256_L128_reluActivation_0.5Dropout_BatchNorm_HE_initialized_88Batch_128Epochs_0.001LR_0.001WD_0.9Momentum_AdamOptimizer1574710711.log"

#Image Classifier
MODEL_LOG = "image_classifier_log/first/CNN_2D/Net__CNN_2D_5KernelSize_C32_C64_C128_L512_L256_reluActivation_0.5Dropout_BatchNorm_HE_initialized_144Batch_10Epochs_0.001LR_0.001WD_0.9Momentum_AdamAmsgradOptimizer.log"
# Grayscale 3 label
MODEL_LOG = "image_classifier_log/as_wav/CNN_2D/GRAYCALE__Net__CNN_2D_5KernelSize_C32_C64_L256_L128_reluActivation_0.5Dropout_BatchNorm_HE_initialized_144Batch_16Epochs_0.001LR_0.001WD_0.9Momentum_AdamOptimizer__3labels.log_1574838980.6495185"

def plot_file(file_path=FILE_PATH):
    print("-------------", file_path)
    df = load_accelerometer_data(macnil_file=False, file_path=file_path, undersample=False)
    print("-------------", df.head())
    plot_dataframe(df)


def plot_dataframe(dataframe:pd.DataFrame, title=FILE_NAME, label="", text=""):

    # JUST SMARTPHONE NOT PRE-PROCESSED
    # plt.plot(dataframe.index, dataframe["x"], color='blue')
    # plt.plot(dataframe.index, dataframe["y"], color='green')
    # plt.plot(dataframe.index, dataframe["z"], color='red')
    # plt.legend(["z", "x", "y"])
    # ---------------------------------------------------------


    plt.plot(dataframe.index, dataframe["x"], color='red')
    plt.plot(dataframe.index, dataframe["y"], color='blue')
    plt.plot(dataframe.index, dataframe["z"], color='green')
    plt.suptitle(label)
    plt.title(title)
    plt.ylabel("acceleration")
    plt.xlabel("time")


    plt.legend(["x", "y", "z"])

    plt.text(30, 0.4, text, horizontalalignment='center',verticalalignment='center')
    plt.show()


def training_plot(file_path, epoch_index):
    df = pd.read_csv(file_path)
    if epoch_index:
        df.set_index('epoch', inplace=True)
    else:
        df.set_index('time')


    max_train_acc = df['train_acc'].max()
    max_val_acc = df['val_acc'].max()
    max_acc_train_idx = df['train_acc'].idxmax()
    max_acc_val_idx = df['val_acc'].idxmax()

    min_train_loss = df['train_loss'].min()
    min_val_loss = df['val_loss'].min()
    min_loss_train_idx = df['train_loss'].idxmin()
    min_loss_val_idx = df['val_loss'].idxmin()

    plt.text(2000, 3, f"Max Train accuracy: {max_train_acc}", horizontalalignment='center', verticalalignment='center')
    plt.text(2000, 2, f"Max Train accuracy: {max_val_acc}", horizontalalignment='center', verticalalignment='center')

    print("Max train acc: ", max_train_acc, " at epoch: ", max_acc_train_idx)
    print("Max val acc: ", max_val_acc, " at epoch: ", max_acc_val_idx)
    print("min train loss: ", min_train_loss, " at epoch: ", min_loss_train_idx)
    print("min val loss: ", min_val_loss, " at epoch: ", min_loss_val_idx)

    #
    # df['train_acc'] = df['train_acc'].ewm(alpha=0.02).mean()  # exponential weighted moving average
    # df['train_loss'] = df['train_loss'].ewm(alpha=0.02).mean()
    #
    # df['val_acc'] = df['val_acc'].ewm(alpha=0.02).mean()
    # df['val_loss'] = df['val_loss'].ewm(alpha=0.02).mean()

    plt.plot(df.index, df["train_acc"], color='blue')
    plt.plot(df.index, df["train_loss"], color='cyan')
    plt.plot(df.index, df["val_acc"], color='red')
    plt.plot(df.index, df["val_loss"], color='orange')
    plt.suptitle(file_path)
    plt.xlabel("epochs")
    plt.legend(["train_acc", "train_loss", "val_acc","val_loss"])
    plt.show()


if __name__ == "__main__":
    # plot_file()
    training_plot(MODEL_LOG, epoch_index=False)