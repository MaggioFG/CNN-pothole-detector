import pandas as pd
import numpy as np
from datetime import datetime
import os


## Default Values
FILE_PATH = "../smartphone_dataset/window-1500-frequency-50/3_label_1/pothole/BIG__LG-H970-2019-10-27 10:53:19.760.csv"
INPUT_PATH = "test_set"
# INPUT_PATH = "macnil_logs/translated_sure_pothole"
UNDERSAMPLE = True
SAMPLING_FREQUENCY = 50

# for each event(==True) we cut the data EVENT_WINDOW_SIZE  Milliseconds before and after that event
EVENT_WINDOW_SIZE = 1500

OUTPUT_FOLDER = os.path.join("..", "smartphone_dataset", "new_smartphone_axes",f"window-{EVENT_WINDOW_SIZE}")
##--------------------------------------------------

def load_accelerometer_data(macnil_file, file_path=FILE_PATH, undersample=UNDERSAMPLE, sampling_frequency=SAMPLING_FREQUENCY):

    try:
        if macnil_file:
            df = pd.read_csv(file_path, skiprows=1, na_values=["NONE", "None"], names=["device_name", "timestamp", "x", "y", "z", "lat", "long","event"])
            df['speed'] = 0.000
            start_time_stamp =  df[df["event"]]["device_name"].astype(dtype=np.int)
            start_time_stamp = pd.to_datetime(start_time_stamp, unit='ms').values
            np_time_offset = df["timestamp"].values
            #print(np_time_offset)
            #df['timestamp'] = pd.to_datetime(df[['timestamp']], unit='ms')
            time_offsets = []
            new_timestamp = []

            for time_off in np_time_offset:
                time_offsets = pd.offsets.Milli(time_off)
                new_timestamp.append(start_time_stamp[0] + time_offsets)

            df['timestamp'] = pd.Series(new_timestamp)
            df['device_name'] = 'SmartbBox'
        else:
            df = pd.read_csv(file_path, na_values=["NONE"])
        if undersample:
            df = undersampler(df, sampling_frequency)
    except:
        print("Error reading: ", file_path)
        exit(1)
    return  df


def undersampler(samples: pd.DataFrame, frequency=SAMPLING_FREQUENCY):
    date_times = []
    for timestamp in samples['timestamp']:
        date_times.append(pd.Timestamp(timestamp, unit='ms'))
    samples['timestamp'] = pd.to_datetime(date_times)
    samples.set_index('timestamp', inplace=True)
    deviceName = samples['device_name'][0]
    samples['event']  = samples['event'].astype(dtype=np.bool)
    time_interval = 1000 // frequency
    time_interval_str = f"{time_interval}ms"
    undersampled_df = samples.resample(time_interval_str).mean().ffill()
    # undersampled_df = samples[['device_name','x', 'y', 'z', 'lat', 'long','speed','event']].resample(time_interval_str).mean().ffill()
    undersampled_df[['x', 'y', 'z', 'speed']] = round(undersampled_df[['x', 'y', 'z', 'speed']], 3)
    undersampled_df[['lat', 'long']] = round(undersampled_df[['lat', 'long']], 7)
    undersampled_df.insert(0, 'device_name', deviceName)
    undersampled_df['event'] = undersampled_df['event'] > 0.0
    return undersampled_df


def event_splitter(entire_log:pd.DataFrame, event_window_size=EVENT_WINDOW_SIZE, event_minimum_distance=1):
    """
    Function to split events (marked as True) inside a csv file
    :param entire_log:
    Log in which to find the events, and cut them apart
    :param event_window_size:
    size of the cut, in Milliseconds.
    will be cut event_window_size before and after the events in all the dataframe
    :param event_minimum_distance:
    the distance in ms, after which two events are considered different(generating separate dataframe to return)
    :return:
     List of tuples (event_timestamp, dataframe) with dataframe containing event_windows_size*2*1000 rows
    """


    time_interval = pd.to_timedelta(pd.infer_freq(entire_log.index)).total_seconds()
    frequency = round(1/time_interval)
    event_dataframes = []
    event_indexes = entire_log.index[entire_log['event']].tolist()
    last_event_index = pd.Timestamp(0, unit='ms')
    for index in event_indexes:
        if index > last_event_index + pd.offsets.Milli(event_minimum_distance*time_interval*1000):
            start_timestamp = entire_log.index.get_loc(index - pd.offsets.Milli(event_window_size), method='nearest')
            end_timestamp = entire_log.index.get_loc(index + pd.offsets.Milli(event_window_size), method='nearest')
            event_dataframe = entire_log.iloc[start_timestamp:end_timestamp]
            if len(event_dataframe) >= round((event_window_size*2*frequency)/1000):
                event_dataframes.append((index, event_dataframe))
        last_event_index = index

    return event_dataframes

def write_dataset(event_frame: pd.DataFrame, is_smartphone,smarphone_position='vertical'):
    time_interval = pd.to_timedelta(pd.infer_freq(event_frame.index)).total_seconds()
    frequency = round(1 / time_interval)
    eventFrames = event_splitter(event_frame)
    event_indexes = [timestamp for timestamp, _ in eventFrames]
    for timestamp, single_eventFrame in eventFrames:
        event_array = single_eventFrame.index.isin(event_indexes)
        event_count = np.sum(event_array)
        date_time = timestamp.strftime("%Y-%m-%d %H:%M:%S.%f")[:-3]
        output_folder = OUTPUT_FOLDER + f"-frequency-{frequency}"
        out_path = os.path.join(output_folder, f"{event_count}", f"{single_eventFrame['device_name'].iloc[0]}-{date_time}.csv")
        if not os.path.exists(os.path.dirname(out_path)):
            os.makedirs(os.path.dirname(out_path))

        if is_smartphone:
            if smarphone_position=='vertical':
                smartphone_df = single_eventFrame[["device_name", "z", "x", "y", "lat", "long", "speed", "event"]]
                out_labels = ['device_name','x', 'y', 'z', 'lat', 'long','speed','event']
                smartphone_df.to_csv(out_path, index_label="UTC_timestamp", header=out_labels, na_rep='NONE', encoding='utf-8')
        else:
            single_eventFrame.to_csv(out_path, index_label="UTC_timestamp", na_rep='NONE', encoding='utf-8')


if __name__ == "__main__":

    # samples.to_csv('temp.csv', na_rep="NONE", encoding='utf-8')
    for dirpath, dirname, filenames in os.walk(INPUT_PATH):
        for file in filenames:
            input_file =  os.path.join(dirpath, file)
            if os.path.basename(input_file).startswith("Crash"):
                macnil_file=True
                is_smartphone=False
            else:
                macnil_file=False
                is_smartphone=True
            
            log_dataframe = load_accelerometer_data(file_path=input_file, macnil_file=macnil_file)
            write_dataset(log_dataframe, is_smartphone=is_smartphone)



    #samples = load_accelerometer_data()
    #write_dataset(samples)
