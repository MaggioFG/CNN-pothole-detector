from typing import Tuple, Dict, List
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

## Default values
NUM_LABEL = 2
DEFAULT_CONV_DICT = {0: 32, 1:64, 2:32, 3:32, 4:32, 5:32}
DEFAULT_LINEAR_DICT = {0: 256, 1:128, 2:128, 3:32, 4:32, 5:32}
DEFAULT_CONVS = [32,64]
# DEFAULT_LINEARS = [512,64, 32, 32, 32, 32]
DEFAULT_LINEARS = [256,128]
## --------------

def conv_block(conv_dimension:str, in_channels, out_channels, nonlinear, dropout_p, *args, **kwargs):
    if conv_dimension.upper() == "1D":
        conv = nn.Conv1d(in_channels, out_channels, *args, **kwargs)
        batch_norm = nn.BatchNorm1d(out_channels)
        max_pooling = nn.MaxPool1d(2,2)
        dropout = nn.Dropout(p=dropout_p)
    elif conv_dimension.upper() == "2D":
        conv = nn.Conv2d(in_channels, out_channels, *args, **kwargs)
        batch_norm = nn.BatchNorm2d(out_channels)
        max_pooling = nn.MaxPool2d(2,2)
        dropout = nn.Dropout2d(p=dropout_p)
    elif conv_dimension.upper() == "3D":
        conv = nn.Conv3d(in_channels, out_channels, *args, **kwargs)
        batch_norm = nn.BatchNorm3d(out_channels)
        max_pooling = nn.MaxPool3d(2,2)
        dropout = nn.Dropout3d(p=dropout_p)
    else:
        print("ERROR: Dimension of convolutional layer not supported")
        exit(1)

    return nn.Sequential(
        conv,
        batch_norm,
        nonlinear(),
        max_pooling,
        dropout
    )

def lin_blocks(in_neurons, out_neurons, nonlinear,dropout_p):
    return nn.Sequential(
        nn.Linear(in_neurons, out_neurons),
        nn.BatchNorm1d(out_neurons),
        nonlinear(),
        nn.Dropout(dropout_p)
    )


class ConvNet(nn.Module):

    def __init__(self, input_sizes:Tuple, conv_dim="2D", kernel_size=5, num_label=NUM_LABEL,
                 num_unit_per_conv_layer:List=DEFAULT_CONVS,
                 num_unit_per_linear_layer:List=DEFAULT_LINEARS,
                 nonlinear='relu', dropout_p=0.0):

        super(ConvNet, self).__init__()

        conv_dim = "1D" if len(input_sizes) == 2 else conv_dim
        print(conv_dim)
        self.net_type = f"CNN_{conv_dim}"
        self.model_name = f"{self.net_type}_{kernel_size}KernelSize_C{'_C'.join([str(num_unit_per_conv_layer[i]) for i in range(len(num_unit_per_conv_layer))])}_" \
                          f"L{'_L'.join([str(num_unit_per_linear_layer[i]) for i in range(len(num_unit_per_linear_layer))])}_{nonlinear}Activation_{dropout_p}Dropout_BatchNorm"

        self.nonlinear = nn.LeakyReLU if nonlinear=='leaky_relu' else nn.ReLU
        self.dropout_p = dropout_p
        channels = input_sizes[0]

        conv_unit_list = [channels, *num_unit_per_conv_layer]

        # if conv_dim.upper() == ""
        conv_blocks = [conv_block(conv_dim, in_ch, out_ch, self.nonlinear, dropout_p , kernel_size=kernel_size)
                       for in_ch, out_ch in zip(conv_unit_list, conv_unit_list[1:])]


        self.conv_layers = nn.Sequential(*conv_blocks)
        # Workaround to know the dimensions after the last conv layer
        self._to_linear_dim = None
        x = torch.randn(np.prod(input_sizes)).view(-1, channels, *input_sizes[1:])
        self.convs(x)
        # ------------------------------------------------------------

        linear_unit_list = [self._to_linear_dim, *num_unit_per_linear_layer]

        linear_layer_list = [lin_blocks(in_neurons, out_neurons, self.nonlinear, dropout_p)
                             for in_neurons, out_neurons in zip(linear_unit_list, linear_unit_list[1:])]

        self.lin_layers = nn.Sequential(*linear_layer_list)
        self.output_l = nn.Linear(num_unit_per_linear_layer[-1], num_label)

        # Weight initialization
        self.conv_layers.apply(init_he_uniform(nonlinear))
        self.lin_layers.apply(init_he_uniform(nonlinear))
        self.output_l.apply(init_he_uniform(nonlinear))

        self.model_name += '_HE_initialized'

    def convs(self, x):
        x = self.conv_layers(x)

        # Workaround to know the dimensions after the last conv layer
        if self._to_linear_dim is None:
            #print("Shape of a X sample: ", x[0].shape)
            self._to_linear_dim = np.prod(x[0].shape)
            # print("Shape after all conv layers", self._to_linear_dim)
        #------------------------------------------------------------
        return x

    def forward(self, plane:torch.Tensor):
        x = self.convs(plane)
        x = x.flatten(start_dim=1)
        x = self.lin_layers(x)
        # x = F.log_softmax(self.output_l(x), dim=1)
        x = self.output_l(x)
        return x


def init_he_uniform(nonlinear):
    # Based on Delving deep into rectifiers: Surpassing human-level performance on ImageNet classification He, K. et al. (2015)
    def weight_initializer(module):
        if isinstance(module, nn.Conv2d) or isinstance(module, nn.Conv1d) or isinstance(module, nn.Linear):
            nn.init.kaiming_uniform_(module.weight, nonlinearity=nonlinear)
            if nonlinear == 'relu':
                module.bias.data.fill_(0.01)

    return weight_initializer
