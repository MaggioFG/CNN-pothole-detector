import os
from typing import Tuple
import numpy as np
import cv2
import torch
import torch.nn as nn
import torch.optim as optim
from time import time
from tqdm import tqdm
from torch.cuda import is_available as cuda_is_available
from neural_networks import ConvNet

#   Default values
SEED = 29
MODEL_NAME = f"image_classifier/logs_model-{int(time())}"
LOG_FOLDER = "image_classifier_log/as_wav"
TRAINED_MODEL_FOLDER = "trained_models"

USE_GPU = True
GRAY_SCALE = False
REBUILD_DATA = False
FORCE_RETRAIN = False
VALIDATION_PCT = 0.2
DATASET_PATH = "image_dataset_"

DEFAULT_CONVS = [32,64]
DEFAULT_LINEARS = [256,128]
# DEFAULT_CONVS = [32,64,96]
# DEFAULT_LINEARS = [512,256]

MOMENTUM = 0.9
WEIGHT_DECAY = 0.001
LEARNING_RATE = 0.001
EPOCHS = 16
BATCH_SIZE = 144
AMSGRAD = False


TEST_STEP = BATCH_SIZE * 10

MAX_SAMPLES_IN_MEMORY = 150


if USE_GPU:
    device = torch.device("cuda:0") if cuda_is_available() else torch.device("cpu")
else:
    device = torch.device("cpu")


if not os.path.exists(LOG_FOLDER):
    os.makedirs(LOG_FOLDER)
if not os.path.exists(TRAINED_MODEL_FOLDER):
    os.makedirs(TRAINED_MODEL_FOLDER)
#-------------------



class ImagePotholeClassifier():
    net = None
    IMG_SIZE = 150
    pothole = "image_dataset/pothole"
    smooth = "image_dataset/smooth"
    shadow = "image_dataset/shadow"
    manhole = "image_dataset/manhole"
    road_marking = "image_dataset/road_marking"
    # label_dict = {smooth:0, pothole:1}
    # label_dict = {pothole: 0, smooth: 1, shadow:2, manhole:3, road_marking:4}
    label_dict = {smooth: 0,  manhole: 1, pothole:2}
    dataset = []
    training_set_X = None
    training_set_y = None
    test_set_X = None
    test_set_y = None
    net = None

    def make_training_data(self, rebuild_data=REBUILD_DATA, val_pct=VALIDATION_PCT, use_gpu=USE_GPU, dataset_path=DATASET_PATH):

        self.val_pct = val_pct
        start_time = time()
        if rebuild_data or not os.path.exists(f"{dataset_path}_samples.npy") or not os.path.exists(f"{dataset_path}_labels.npy"):
            for label in self.label_dict:
                print(label)
                for image in os.listdir(label):
                    try:
                        img_path = os.path.join(label, image)
                        if GRAY_SCALE:
                            img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
                            img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
                            img = np.reshape(img, (1, self.IMG_SIZE, self.IMG_SIZE))
                        else:
                            img = cv2.imread(img_path, cv2.IMREAD_COLOR)
                            img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
                            img = np.reshape(img, (img.shape[-1], *img.shape[:2]))
                        self.dataset.append([np.array(img), np.eye(len(self.label_dict))[self.label_dict[label]]])
                        # if label == self.cat_path:
                        #     self.cat_count += 1
                        # if label == self.dog_path:
                        #     self.dog_count += 1
                    except Exception as e:
                        print(str(e), "\n Error with image: ", img_path)

            np.random.seed(SEED)
            np.random.shuffle(self.dataset)
            self.samples = np.array([i[0] for i in self.dataset])
            self.samples = self.samples / 255.0
            self.labels = np.array([i[1] for i in self.dataset])

            np.save(f"{dataset_path}_samples.npy", self.samples)
            np.save(f"{dataset_path}_labels.npy", self.labels)

            # print("dim1 ", len(self.dataset))
            # print("dim2 ", len(self.dataset[0]))
            # print("dim3 ", len(self.dataset[0][0]))
            # self.dataset = np.ndarray(self.dataset)
            #
            # np.save(dataset_path, self.dataset)
                # print("Number of cats: ", self.cat_count)
                # print("Number of dogs: ", self.dog_count)
        else:
            self.samples = np.load(f"{dataset_path}_samples.npy", allow_pickle=True)
            self.labels = np.load(f"{dataset_path}_labels.npy", allow_pickle=True)


        self._load_tensors()


        print("Samples shape = ", self.samples.shape)
        print("labels shape = ", self.labels.shape)

        print(f"Time to load all samples: {int(time()-start_time)}s")
        # X = torch.Tensor([i[0] for i in self.dataset]).view(-1, *self.dataset[0][0].shape)
        # X = torch.Tensor([i[0] for i in self.dataset]).view(-1, img.shape[0], *img.shape[1:])
        # print("Tensor x shape: ", X.shape)
        # X = X / 255.0
        # # y = torch.Tensor([i[1] for i in self.dataset])
        # y = torch.tensor([i[1] for i in self.dataset], dtype=torch.long)  # .view(-1, 1, 2)
        # val_size = int(len(X) * val_pct)
        #
        # self.train_X = X[:-val_size]
        # self.train_y = y[:-val_size]
        #
        # self.test_X = X[-val_size:]
        # self.test_y = y[-val_size:]
        #
        # if use_gpu:
        #     self.train_X = self.train_X.pin_memory()
        #     self.train_y = self.train_y.pin_memory()
        #
        #     self.test_X = self.test_X.pin_memory()
        #     self.test_y = self.test_y.pin_memory()

    def _load_tensors(self, use_gpu=USE_GPU):
        if not self.samples is None:
            total_samples = len(self.samples)
            test_set_samples = int(total_samples * self.val_pct)
            print("Total number of samples: ", total_samples)

            X = torch.Tensor(self.samples).view(-1, *self.get_tensor_sizes())
            y = torch.tensor(self.labels, dtype=torch.long)

            self.training_set_X = X[:-test_set_samples]
            self.training_set_y = y[:-test_set_samples]

            self.test_set_X = X[-test_set_samples:]
            self.test_set_y = y[-test_set_samples:]

            print("Training sample length: ", len(self.training_set_X))
            print("Test set length: ", len(self.test_set_y))

            if use_gpu:
                self.training_set_X = self.training_set_X.pin_memory()
                self.training_set_y = self.training_set_y.pin_memory()

                self.test_set_X = self.test_set_X.pin_memory()
                self.test_set_y = self.test_set_y.pin_memory()

            return

        else:
            print("Error: load the dataset first with load_dataset_from_files()")
            exit(1)

    def _fwd_pass(self, X, y, train=False):
        X = X.to(device)
        y = y.to(device)

        if train:
            self.optimizer.zero_grad()

        output = self.net(X)

        matches = [torch.argmax(i) == torch.argmax(j) for i, j in zip(output, y)]
        acc = matches.count(True) / len(matches)
        # loss = self.criterion(output, y)
        loss = self.criterion(output, torch.argmax(y, dim=1))

        if train:
            loss.backward()
            self.optimizer.step()
        return acc, loss

    def set_model_name(self, net: nn.Module, amsgrad:bool=AMSGRAD,batch_size=BATCH_SIZE, epochs=EPOCHS, lr=LEARNING_RATE, wd=WEIGHT_DECAY, momentum=MOMENTUM):
        self.model_name = "GRAYCALE__" if GRAY_SCALE else "COLOR__"
        self.model_name += f"Net__{net.model_name}_{batch_size}Batch_{epochs}Epochs_{lr}LR_{wd}WD_{momentum}Momentum"
        print("SETNAME_AMSGRAD: ", amsgrad)
        if amsgrad:
            self.model_name += f"_AdamAmsgradOptimizer__{len(self.label_dict)}labels"
        else:
            self.model_name += f"_AdamOptimizer__{len(self.label_dict)}labels"

    def initialize_model(self, net: nn.Module, lr=LEARNING_RATE, wd=WEIGHT_DECAY, amsgrad=AMSGRAD):
        self.net = net
        self.optimizer = optim.Adam(self.net.parameters(), lr=lr, amsgrad=amsgrad, weight_decay=wd)
        self.criterion = nn.CrossEntropyLoss()

    def train(self, net: nn.Module, batch_size=BATCH_SIZE, epochs=EPOCHS, test_step=TEST_STEP,
              lr=LEARNING_RATE, wd=WEIGHT_DECAY, momentum=MOMENTUM, amsgrad=AMSGRAD):
        print("TRAIN AMSGRAD: ", amsgrad)
        self.initialize_model(net, lr, wd, amsgrad)
        self.set_model_name(net, amsgrad, batch_size, epochs, lr, wd, momentum)
        # self.model_name = f"Net__{net.model_name}_{batch_size}Batch_{epochs}Epochs_{lr}LR_{wd}WD_{momentum}Momentum_AdamOptimizer{int(time())}"
        self.net.train(True)
        if not self.dataset is None:
            filepath = os.path.join(LOG_FOLDER, net.net_type, f"{self.model_name}.log")
            if not os.path.exists(os.path.dirname(filepath)):
                os.makedirs(os.path.dirname(filepath))
            with open(f"{filepath}_{time()}", "a") as f:
                f.write("time,train_acc,train_loss,val_acc,val_loss,epoch\n")
                # self.optimizer = optim.Adam(self.net.parameters(), lr=lr, amsgrad=False, weight_decay=wd)
                # self.optimizer = optim.SGD(self.net.parameters(), momentum=momentum, lr=lr, weight_decay=wd)
                #
                # Just to plot the overfitting and choosing the best epoch to stop
                # self.optimizer = optim.SGD(self.net.parameters(), lr=lr)

                # ---------------------------------------------------------------
                # self.criterion = nn.CrossEntropyLoss()
                self.net.to(device)
                total_time = 0
                for epoch in tqdm(range(epochs)):
                    epoch_loss = 0
                    num_of_batch = 0
                    start = time()
                    for i in range(0, len(self.training_set_X), batch_size):
                        # print("Batch Number: ", num_of_batch)
                        num_of_batch += 1
                        batch_X = self.training_set_X[i: i + batch_size].view(-1, *self.get_tensor_sizes())
                        batch_y = self.training_set_y[i: i + batch_size]

                        # print("lenght of batch: ", len(batch_y))
                        acc, loss = self._fwd_pass(batch_X, batch_y, train=True)

                        epoch_loss += loss.item()
                        if test_step != 0 and i % test_step == 0:
                            val_acc, val_loss = self.test(size=100)
                            f.write(
                                f"{round(time(), 3)},{round(float(acc), 2)},{round(float(loss.item()), 4)},{round(float(val_acc), 2)}, {round(float(val_loss.item()), 4)},{epoch} \n")

                            # Human readable
                            # f.write(f"{self.model_name}-{round(time(), 3)}; \n"
                            # f"Trainig set  Accuracy and Loss: {round(float(acc), 2)}, {round(float(loss), 4)}, \n"
                            # f"Test set Accuracy and Loss: {round(float(val_acc), 2)}, {round(float(val_loss), 4)}, \n")

                    epoch_time = time() - start
                    total_time += epoch_time
                    # print(f"\nLoss at {epoch} epoch: {loss} \n train took: {epoch_time: .2f} \n")

                print(f"Complete train took: {total_time: .2f}")
        else:
            print("ERROR: load the dataset first with make_training_data()")
            exit(1)

    def confusion_matrix(self, test_set_X=None, test_set_y=None, test_set_linear=None, size="max"):
        if self.net is None:
            print("ERROR: NO net has ever been trained")
            exit(1)

        if test_set_X is None or test_set_y is None:
            test_set_X = self.test_set_X
            test_set_y = self.test_set_y

        if size == "max" or size > len(test_set_X):
            size = len(test_set_X)
            # print("Testing on max number of sample available: ", size, " samples")
        random_start = 0
        if size != len(test_set_X):
            random_start = np.random.randint(len(test_set_X) - size)
        self.net.train(False)
        with torch.no_grad():
            # If we are predicting three classes, (0 = smooth, 1 = manhole, 2 = pothole)

            if size > MAX_SAMPLES_IN_MEMORY:
                batch_size = MAX_SAMPLES_IN_MEMORY

                if test_set_y.shape[-1] == 3:
                    count_t_smooth = 0
                    count_t_manhole = 0
                    count_t_pothole = 0

                    count_f_smooth_was_manhole = 0
                    count_f_smooth_was_pothole = 0
                    count_f_manhole_was_smooth = 0
                    count_f_manhole_was_pothole = 0
                    count_f_pothole_was_smooth = 0
                    count_f_pothole_was_manhole = 0

                    for i in tqdm(range(0, size, batch_size)):
                        batch_X = test_set_X[i: i + batch_size].view(-1, *self.get_tensor_sizes())
                        batch_y = test_set_y[i: i + batch_size]
                        batch_X, batch_y = batch_X.to(device), batch_y.to(device)
                        output = self.net(batch_X)

                        t_smooth = [torch.argmax(i) == 0 and torch.argmax(j) == 0 for i, j in zip(output, batch_y)]
                        t_manhole = [torch.argmax(i) == 1  and torch.argmax(j) == 1 for i, j in zip(output, batch_y)]
                        t_pothole = [torch.argmax(i) == 2  and torch.argmax(j) == 2 for i, j in zip(output, batch_y)]

                        f_smooth_was_manhole = [torch.argmax(i) == 0  and torch.argmax(j) == 1 for i, j in zip(output, batch_y)]
                        f_smooth_was_pothole = [torch.argmax(i) == 0  and torch.argmax(j) == 2 for i, j in zip(output, batch_y)]

                        f_manhole_was_smooth = [torch.argmax(i) == 1 and torch.argmax(j) == 0 for i, j in zip(output, batch_y)]
                        f_manhole_was_pothole = [torch.argmax(i) == 1 and torch.argmax(j) == 2 for i, j in zip(output, batch_y)]


                        f_pothole_was_smooth = [torch.argmax(i) == 2  and torch.argmax(j) == 0 for i, j in zip(output, batch_y)]
                        f_pothole_was_manhole = [torch.argmax(i) == 2  and torch.argmax(j) == 1 for i, j in zip(output, batch_y)]



                        count_t_smooth += t_smooth.count(True)
                        count_t_manhole += t_manhole.count(True)
                        count_t_pothole += t_pothole.count(True)

                        count_f_smooth_was_manhole += f_smooth_was_manhole.count(True)
                        count_f_smooth_was_pothole += f_smooth_was_pothole.count(True)
                        count_f_manhole_was_smooth += f_manhole_was_smooth.count(True)
                        count_f_manhole_was_pothole += f_manhole_was_pothole.count(True)
                        count_f_pothole_was_smooth += f_pothole_was_smooth.count(True)
                        count_f_pothole_was_manhole += f_pothole_was_manhole.count(True)


                    print("Num of samples: ", len(test_set_y))
                    print("count_t_smooth: ", count_t_smooth)
                    print("count_t_manhole: ",count_t_manhole)
                    print("count_t_pothole: ",count_t_pothole)

                    print("count_f_smooth_was_manhole: ", count_f_smooth_was_manhole)
                    print("count_f_smooth_was_pothole: ", count_f_smooth_was_pothole)
                    print("count_f_manhole_was_smooth: ", count_f_manhole_was_smooth)
                    print("count_f_manhole_was_pothole: ", count_f_manhole_was_pothole)
                    print("count_f_pothole_was_smooth: ", count_f_pothole_was_smooth)
                    print("count_f_pothole_was_manhole: ", count_f_pothole_was_manhole)
                # Binary classification smooth = 0, pothole = 1
                elif test_set_y.shape[-1] == 2:
                    tp_count = 0
                    fn_count = 0
                    fp_count = 0
                    tn_count = 0

                    for i in tqdm(range(0, size, batch_size)):
                        batch_X = test_set_X[i: i + batch_size].view(-1, *self.get_tensor_sizes())
                        batch_y = test_set_y[i: i + batch_size]
                        batch_X, batch_y = batch_X.to(device), batch_y.to(device)
                        output = self.net(batch_X)
                        tp = [torch.argmax(i) == torch.argmax(j) and torch.argmax(j) == 1 for i, j in zip(output, batch_y)]
                        fp = [torch.argmax(i) != torch.argmax(j) and torch.argmax(j) == 0 for i, j in zip(output, batch_y)]
                        fn = [torch.argmax(i) != torch.argmax(j) and torch.argmax(j) == 1 for i, j in zip(output, batch_y)]
                        tn = [torch.argmax(i) == torch.argmax(j) and torch.argmax(j) == 0 for i, j in zip(output, batch_y)]

                        tp_count += tp.count(True)
                        fn_count += fn.count(True)
                        fp_count += fp.count(True)
                        tn_count += tn.count(True)

                    print("Num of samples: ", len(test_set_y))
                    print("True positive: ", tp_count)
                    print("False positive: ", fp_count)
                    print("True negative: ", tn_count)
                    print("False negative: ", fn_count)


            else:
                X, y = test_set_X[random_start: random_start + size], test_set_y[random_start: random_start + size]


        return

    def test(self, test_set_X=None, test_set_y=None, test_set_linear=None, size="max"):
        if self.net is None:
            print("ERROR: NO net has ever been trained")
            exit(1)

        if test_set_X is None or test_set_y is None:
            test_set_X = self.test_set_X
            test_set_y = self.test_set_y

        if size == "max" or size > len(test_set_X):
            size = len(test_set_X)
            # print("Testing on max number of sample available: ", size, " samples")

        self.net.train(False)
        with torch.no_grad():

            if size > MAX_SAMPLES_IN_MEMORY:
                tot_acc = 0
                tot_loss = 0
                num_batches = 0
                batch_size = MAX_SAMPLES_IN_MEMORY
                for i in tqdm(range(0, size, batch_size)):
                    num_batches += 1
                    batch_X = test_set_X[i: i + batch_size].view(-1, *self.get_tensor_sizes())
                    batch_y = test_set_y[i: i + batch_size]

                    # print("lenght of batch: ", len(batch_y))
                    acc, loss = self._fwd_pass(batch_X, batch_y, train=False)
                    tot_acc += acc
                    tot_loss += loss

                val_acc = tot_acc / num_batches
                val_loss = tot_loss / num_batches

            else:
                random_start = 0
                if size != len(test_set_X):
                    random_start = np.random.randint(len(test_set_X) - size)

                X, y = test_set_X[random_start: random_start + size], test_set_y[random_start: random_start + size]

                # start = time()
                val_acc, val_loss = self._fwd_pass(X.view(-1, *self.get_tensor_sizes()), y, train=False)
                # print(f"Predictions on TestSet took: {time() - start: .2f}")
                # print("Accuracy on TestSet: ", val_acc)

            return val_acc, val_loss


    def get_tensor_sizes(self) -> Tuple[int, ...]:
        if not self.training_set_X is None:
            return self.training_set_X[0].shape
        else:
            return self.samples[0].shape

    def set_net(self, net: nn.Module):
        self.net = net


if __name__ == '__main__':
    classifier = ImagePotholeClassifier()
    classifier.make_training_data()


    conv_net_2d = ConvNet(input_sizes=classifier.get_tensor_sizes(), dropout_p=0.5, nonlinear='relu',
                          num_unit_per_conv_layer=DEFAULT_CONVS, num_unit_per_linear_layer=DEFAULT_LINEARS, num_label=len(classifier.label_dict))
    classifier.set_model_name(net=conv_net_2d, batch_size=BATCH_SIZE, epochs=EPOCHS, lr=LEARNING_RATE, wd=WEIGHT_DECAY, momentum=MOMENTUM, amsgrad=AMSGRAD)
    classifier.initialize_model(net=conv_net_2d,lr=LEARNING_RATE,wd=WEIGHT_DECAY, amsgrad=AMSGRAD)

    model_path = os.path.join(TRAINED_MODEL_FOLDER,classifier.model_name)
    if not os.path.exists(model_path) or FORCE_RETRAIN:
        print("training new model: ", classifier.model_name)
        with open(model_path, 'wb') as f:
            classifier.train(conv_net_2d)
            torch.save(conv_net_2d,f)
    else:
        print("loaded trained model: ", classifier.model_name)
        with open(model_path, 'rb') as f:
            saved_model = torch.load(model_path)
            classifier.set_net(saved_model)

    print("------------MODEL----------------")
    print(classifier.net)

    acc, loss = classifier.test(test_set_X=classifier.training_set_X, test_set_y=classifier.training_set_y, size="max")
    val_acc, val_loss = classifier.test(size="max")
    print("Train Acc:", acc)
    print("Train Loss:", loss.item())
    print("Test Acc:", val_acc)
    print("Test Loss:", val_loss.item())
    classifier.confusion_matrix(size="max")
