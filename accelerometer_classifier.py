from typing import Tuple
import pandas as pd
import os
import numpy as np
import torch
from neural_networks import *
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
from time import time, sleep
from tqdm import tqdm
from torch.cuda import is_available as cuda_is_available
import pywt
import scaleogram as scg
import data_visualization
import matplotlib.pyplot as plt


##Default values
DATASET_FOLDER = "accelerometer_dataset/new_smartphone_axes/window-1500-frequency-50/3_label_2_manh_as_poth"
# DATASET_FOLDER = "../smartphone_dataset/new_smartphone_axes/window-1500-frequency-50/smartbox"
# DATASET_FOLDER = "../smartphone_dataset/window-1500-frequency-50/3_label_1"
LOG_FOLDER = "model_logs/the_end/wavelets/"
FEATURES = ("x", "y", "z")
NORMALIZE = True
SEED = 4
DEBUG = True

# WAVELET_NAME = "gaus3" #"rbio3.1" #"morl"
WAVELET_NAME = "fbsp" ##88% acc
# WAVELET_NAME = "cgau7"
# Higher scale-factor (longer wavelet) corresponds with a smaller frequency, so by scaling the wavelet
# in the time-domain we will analyze smaller frequencies (achieve a higher resolution) in the frequency domain. And vice versa
SCALES_BOTTOM = 1
SCALES_CEIL = 151

class_dict = {"smooth" : 0,
              "pothole" : 1}

num_class_dict = {0 : "smooth" ,
              1 : "pothole"}

# class_dict = {"smooth" : 0,
#               "manhole" : 1,
#               "pothole" : 2}
#
# num_class_dict = {0 : "smooth" ,
#               1 : "manhole",
#               2 : "pothole"}

VALIDATION_PCT = 0.2
USE_GPU = True
EPOCHS = 130
BATCH_SIZE = 88
LEARNING_RATE = 0.001
WEIGHT_DECAY = 0.001
MOMENTUM = 0.9
TEST_STEP = 1000 # being test_step > len(training_sample), will test "only" every epoch(when i == 0)
NUM_LABEL = 2

DEFAULT_CONV_DICT = {0: 32, 1:64, 2:32, 3:32, 4:32, 5:32}
DEFAULT_LINEAR_DICT = {0: 256, 1:128, 2:128, 3:32, 4:32, 5:32}
DEFAULT_CONVS = [32,64]
DEFAULT_LINEARS = [256,128]
# DEFAULT_CONVS = [16,32,64]



MODEL_NAME = f"ConvNet_c32_c64_l512__1000epoch_{time()}"



if USE_GPU:
    device = torch.device("cuda:0") if cuda_is_available() else torch.device("cpu")
else:
    device = torch.device("cpu")

if not os.path.exists(LOG_FOLDER):
    os.makedirs(LOG_FOLDER)
##-----------------------


class PotholeClassifier():
    dataset = None
    dataframe = pd.DataFrame()
    val_pct = None
    cwt_samples = None
    samples = None
    linear_features = None
    added_features = None
    labels = None
    training_set_X = None
    training_set_lin_feat = None
    training_set_y = None
    test_set_X = None
    test_set_lin_feat = None
    test_set_y = None
    net = None
    applied_preprocessing = ""

    def load_dataset_from_files(self, dataset_folder=DATASET_FOLDER, features=FEATURES, val_pct=VALIDATION_PCT):

        self.val_pct = val_pct
        self.dataset = []

        i = 0
        for dirpath, dirnames, files in os.walk(dataset_folder):
            for file in files:
                label = os.path.basename(dirpath)
                i += 1
                file_path = os.path.join(dirpath, file)
                df = pd.read_csv(file_path, na_values=["NONE"])
                training_sample = df[[*features]]

                # self.dataframe = pd.concat([self.dataframe, training_sample])
                # smooth = [1, 0], pothole [0, 1]
                self.dataset.append([training_sample.values, np.eye(len(class_dict))[class_dict[label]]])



        np.random.seed(SEED)
        np.random.shuffle(self.dataset)

        self.samples = np.array([i[0] for i in self.dataset]).transpose((0,2,1))
        self.labels = [i[1] for i in self.dataset]

        return


    def min_max_norm(self):
        # MIN-MAX Normalization
        self.samples = np.abs(self.samples)
        self.samples = (self.samples - self.samples.min()) / (self.samples.max() - self.samples.min())
        self.applied_preprocessing += "_Min-Max Normalization"

    def zero_mean_norm(self):
        self.samples = np.abs(self.samples)
        self.samples = (self.samples - self.samples.mean()) / self.samples.std()
        self.applied_preprocessing += "_Zero-mean Normalization"
        if not self.added_features is None:
            self.added_features = (self.added_features - self.added_features.mean()) / self.added_features.std()
        print("ZERO MEAN NORMALIZED: ", np.mean(self.samples))



    def dataset_to_wavelet(self, wavelet_name=WAVELET_NAME, scales_bottom=SCALES_BOTTOM, scales_ceil=SCALES_CEIL):
        if not self.dataset is None:

            # dataset_size = len(self.dataset)
            dataset_size = self.samples.shape[0]
            num_of_axes = len(FEATURES)
            print("NORMAL SAMPLES SHAPE: ", self.samples.shape)
            scales = range(scales_bottom, scales_ceil)
            cwt_samples = np.ndarray(shape=(dataset_size, num_of_axes, scales_ceil - scales_bottom, *self.samples[0].shape[1:]))
            print("CWT SHAPE: ", cwt_samples.shape)
            # print(cwt_samples.shape)

            for sample_index in tqdm(range(0, dataset_size)):
                for component_index in range(0, num_of_axes):
                    signal = self.samples[sample_index, component_index, :]
                    wavelet_transf, _ = pywt.cwt(signal, scales, wavelet_name, 1)
                    cwt_samples[sample_index, component_index, :, :] = wavelet_transf

            self.samples = cwt_samples
            self.applied_preprocessing += f"_Wavelet-{wavelet_name}"


            return
        else:
            print("Error: load the dataset first with load_dataset_from_files()")
            exit(1)

    def per_sample_power(self):
        power_samples = np.zeros(shape=(self.samples.shape[0], 3))
        for sample_idx in range(self.samples.shape[0]):
            power_samples[sample_idx,:] = ((self.samples[sample_idx,:,:]**2).sum(axis=1)) / self.samples.shape[2]

        self.added_features = power_samples
        self.applied_preprocessing += "_Signal Power"
        return


            # for axes in sample:
            #     sample_power = (axes ** 2).sum() / self.samples.shape[2]
            #     power_list.append()
            
    # def add_signal_power(self):
    #
    #
    #     print(self.samples.shape)
    #     num_of_axes = self.samples.shape[1]
    #     #power_columns = np.zeros(shape=(self.samples.shape[0], num_of_axes, self.samples.shape[2]))
    #     power_samples = np.zeros(shape=(self.samples.shape[0], num_of_axes*2, *self.samples.shape[2:]))
    #
    #
    #     # We first copy the old columns
    #     power_samples[:,:num_of_axes, :] = self.samples[:, :, :]
    #     for sample_idx in range(self.samples.shape[0]):
    #         for axes_idx in range(num_of_axes):
    #             power_samples[sample_idx,axes_idx + 3, :] = ((self.samples[sample_idx, axes_idx, :] ** 2).sum()) / self.samples.shape[2]
    #
    #     self.samples = power_samples
    #     self.applied_preprocessing.append('Signal_Power')
    #
    #     return




        #         print("-------------------------------------------------")
        #         print(power_samples.shape)
        #         print(power_samples.sum())
        #         print("-------------------------------------------------")
        #
        # print("-------------------------------------------------")
        # print(np.count_nonzero(power_samples))
        # print("-------------------------------------------------")

        # for sample_index in range (len(self.samples)):
        #    for i in range(self.samples.shape[-1]):
        #        power_samples[i]


    def get_applied_preprocessing(self):
        return self.applied_preprocessing

    def _load_tensors(self, use_gpu=USE_GPU):
        if not self.dataset is None:
            total_samples = len(self.dataset)
            test_set_samples = int(total_samples * self.val_pct)
            print("Total number of samples: ", total_samples)
            print("Training with signals preprocessed with: ", self.applied_preprocessing)

            X = torch.Tensor(self.samples).view(-1, *self.get_tensor_sizes())
            y = torch.tensor(self.labels, dtype=torch.long)

            self.training_set_X = X[:-test_set_samples]
            self.training_set_y = y[:-test_set_samples]

            self.test_set_X = X[-test_set_samples:]
            self.test_set_y = y[-test_set_samples:]

            print("Training sample length: ", len(self.training_set_X))
            print("Test set length: ", len(self.test_set_y))


            if self.net.net_type.startswith("Hybrid"):
                if not self.added_features is None:
                    linear_feature = torch.Tensor(self.added_features)
                    self.training_set_lin_feat = linear_feature[:-test_set_samples]
                    self.test_set_lin_feat = linear_feature[-test_set_samples:]
                else:
                    print("ERROR: using an hybrid CNN, but no linear features were found")
                    exit(1)
            elif self.net.net_type.startswith("CNN"):
                if not self.added_features is None:
                    print("WARNING: linear features have been added to samples, but a not Hybrid CNN has been used")
                    print(" The linear features added will be ignored")
            elif self.net.net_type.startswith("NN"):
                if not self.added_features is None:
                    original_shape = self.training_set_X[0].shape[1:]
                    x_train = torch.flatten(self.training_set_X, start_dim=1)
                    x_test = torch.flatten(self.test_set_X, start_dim=1)
                    lin_train_features = torch.Tensor(self.added_features[:-test_set_samples]).flatten(start_dim=1)
                    lin_test_features = torch.Tensor(self.added_features[-test_set_samples:]).flatten(start_dim=1)
                    self.training_set_X = torch.cat((x_train, lin_train_features), dim=1)
                    self.test_set_X = torch.cat((x_test, lin_test_features), dim=1)


            if use_gpu:
                self.training_set_X = self.training_set_X.pin_memory()
                self.training_set_y = self.training_set_y.pin_memory()

                self.test_set_X = self.test_set_X.pin_memory()
                self.test_set_y = self.test_set_y.pin_memory()

                if not self.training_set_lin_feat is None:
                    self.training_set_lin_feat = self.training_set_lin_feat.pin_memory()
                    self.test_set_lin_feat = self.test_set_lin_feat.pin_memory()

            return
        else:
            print("Error: load the dataset first with load_dataset_from_files()")
            exit(1)

    def _fwd_pass(self, X, y, linear_features=None, train=False):
        X = X.to(device)
        y = y.to(device)
        if not linear_features is None:
            linear_features = linear_features.to(device)

        #print("X shape: ", X.shape)
        #print("y shape: ", y.shape)
        if train:
            self.optimizer.zero_grad()


        output = self.net(X) if linear_features is None else self.net(X, linear_features)
        

        matches = [torch.argmax(i) == torch.argmax(j) for i, j in zip(output, y)]
        acc = matches.count(True) / len(matches)
        # loss = self.criterion(output, y)
        loss = self.criterion(output, torch.argmax(y,dim=1))

        if train:
            loss.backward()
            self.optimizer.step()
        return acc, loss


    def train(self, net:nn.Module, batch_size=BATCH_SIZE, epochs=EPOCHS, test_step=TEST_STEP, log_name=MODEL_NAME, lr = LEARNING_RATE, wd = WEIGHT_DECAY, momentum = MOMENTUM):
        self.net = net
        self.model_name = f"{self.applied_preprocessing}__Pre-process__"
        #self.model_name = "".join([str(named_modules) for named_modules, _ in self.net.named_modules()])
        # lr = 0.001
        # wd = 0.0005
        # wd = 0.001
        # momentum = 0.9
        self.model_name += f"Net__{net.model_name}_{batch_size}Batch_{epochs}Epochs_{lr}LR_{wd}WD_{momentum}Momentum_AdamOptimizer{int(time())}"
        self.net.train(True)
        self._load_tensors()
        if not self.dataset is None:
            filepath = os.path.join(LOG_FOLDER, net.net_type, f"{self.model_name}.log")
            if not os.path.exists(os.path.dirname(filepath)):
                os.makedirs(os.path.dirname(filepath))
            with open(filepath, "a") as f:
                f.write("time,train_acc,train_loss,val_acc,val_loss,epoch\n")
                self.optimizer = optim.Adam(self.net.parameters(), lr=lr, amsgrad=False, weight_decay=wd)
                # self.optimizer = optim.SGD(self.net.parameters(), momentum=momentum, lr=lr, weight_decay=wd)
                #
                # Just to plot the overfitting and choosing the best epoch to stop
                # self.optimizer = optim.SGD(self.net.parameters(), lr=lr)

                # ---------------------------------------------------------------

                self.criterion = nn.CrossEntropyLoss()
                self.net.to(device)
                total_time = 0
                for epoch in tqdm(range(epochs)):
                    epoch_loss = 0
                    num_of_batch = 0
                    start = time()
                    for i in range(0, len(self.training_set_X), batch_size):
                        # print("Batch Number: ", num_of_batch)
                        num_of_batch += 1
                        batch_X = self.training_set_X[i: i + batch_size].view(-1, *self.get_tensor_sizes())
                        batch_y = self.training_set_y[i: i + batch_size]

                        # print("lenght of batch: ", len(batch_y))
                        if  not self.training_set_lin_feat is None:
                            batch_linear_features = self.training_set_lin_feat[i:i+batch_size]
                            acc, loss = self._fwd_pass(batch_X, batch_y, linear_features=batch_linear_features, train=True)
                        else:
                            acc, loss = self._fwd_pass(batch_X, batch_y, train=True)

                        epoch_loss += loss.item()
                        if  test_step != 0 and i % test_step == 0:
                            val_acc, val_loss = self.test()
                            f.write(f"{round(time(), 3)},{round(float(acc), 2)},{round(float(epoch_loss), 4)},{round(float(val_acc), 2)}, {round(float(val_loss.item()),4)},{epoch} \n")

                            # Human readable
                            # f.write(f"{self.model_name}-{round(time(), 3)}; \n"
                            # f"Trainig set  Accuracy and Loss: {round(float(acc), 2)}, {round(float(loss), 4)}, \n"
                            # f"Test set Accuracy and Loss: {round(float(val_acc), 2)}, {round(float(val_loss), 4)}, \n")

                    epoch_time = time() - start
                    total_time += epoch_time
                    #print(f"\nLoss at {epoch} epoch: {loss} \n train took: {epoch_time: .2f} \n")

                print(f"Complete train took: {total_time: .2f}")
        else:
            print("ERROR: load the dataset first with load_dataset_from_files()")
            exit(1)


    def confusion_matrix(self, test_set_X=None, test_set_y=None, test_set_linear=None, size="max"):
        if self.net is None:
            print("ERROR: NO net has ever been trained")
            exit(1)

        if test_set_X is None or test_set_y is None:
            test_set_X = self.test_set_X
            test_set_y = self.test_set_y
            if not self.test_set_lin_feat is None:
                test_set_linear = self.test_set_lin_feat

        if size == "max" or size > len(test_set_X):
            size = len(test_set_X)
            # print("Testing on max number of sample available: ", size, " samples")

        random_start = 0
        if size != len(test_set_X):
            random_start = np.random.randint(len(test_set_X) - size)

        X, y = test_set_X[random_start: random_start + size], test_set_y[random_start: random_start + size]
        if not test_set_linear is None:
            linear = test_set_linear[random_start: random_start + size]
        self.net.train(False)
        with torch.no_grad():
            X, y = X.to(device), y.to(device)
            output = self.net(X)
            tp = [ torch.argmax(i) == torch.argmax(j) and torch.argmax(j) == 1 for i,j in zip (output,y)]
            fp = [ torch.argmax(i) != torch.argmax(j) and torch.argmax(j) == 0 for i,j in zip (output,y)]
            fn = [ torch.argmax(i) != torch.argmax(j) and torch.argmax(j) == 1 for i,j in zip (output,y)]
            tn = [ torch.argmax(i) == torch.argmax(j) and torch.argmax(j) == 0 for i,j in zip (output,y)]


            tp_count = tp.count(True)
            fn_count = fn.count(True)
            fp_count = fp.count(True)
            tn_count = tn.count(True)


        print("Num of samples: ", len(X))
        print("True positive: ", tp_count)
        print("False positive: ", fp_count)
        print("True negative: ", tn_count)
        print("False negative: ", fn_count)

        return tp_count, fp_count


    def test(self, test_set_X=None, test_set_y=None, test_set_linear=None, size="max"):
        if self.net is None:
            print("ERROR: NO net has ever been trained")
            exit(1)

        if test_set_X is None or test_set_y is None:
            test_set_X = self.test_set_X
            test_set_y = self.test_set_y
            if not self.test_set_lin_feat is None:
                test_set_linear = self.test_set_lin_feat


        if size == "max" or size > len(test_set_X):
            size = len(test_set_X)
            #print("Testing on max number of sample available: ", size, " samples")

        random_start = 0
        if size != len(test_set_X):
            random_start = np.random.randint(len(test_set_X) - size)

        X, y = test_set_X[random_start: random_start+size], test_set_y[random_start: random_start+size]
        if not test_set_linear is None:
            linear = test_set_linear[random_start: random_start+size]
        self.net.train(False)
        with torch.no_grad():
            #start = time()
            if test_set_linear is None:
                val_acc, val_loss = self._fwd_pass(X.view(-1, *self.get_tensor_sizes()), y, train=False)
            else:
                val_acc, val_loss = self._fwd_pass(X.view(-1, *self.get_tensor_sizes()), y,linear_features=linear, train=False)

        #print(f"Predictions on TestSet took: {time() - start: .2f}")
        #print("Accuracy on TestSet: ", val_acc)

        return val_acc, val_loss


    def get_tensor_sizes(self) -> Tuple[int, ...] :
        if not self.training_set_X is None:
            return self.training_set_X[0].shape
        else:
            return self.samples[0].shape

    def view_sample(self, sample_index=0, wavelet=False):

        label = num_class_dict[np.argmax(self.labels[sample_index])]
        if wavelet:
            sample = np.transpose(self.samples[sample_index])
            z_wave = sample[:,:,2]
            scales = z_wave[:,0] *100
            scales = np.logspace(scales[1],scales[-1], num=len(scales), dtype=np.int32)
            x_plot_ax = np.arange(sample.shape[1])
            y_values = z_wave[0,:]
            ax = scg.cws(x_plot_ax, y_values, scales, yscale="log")
            ax.set_title(f"Wavelet transform of a {label}")
            ticks = ax.set_yticks([2, 4, 8, 16, 32])
            ticks = ax.set_yticklabels([2, 4, 8, 16, 32])

            plt.axes(ax)
            plt.show()
        else:
            sample = np.transpose(self.samples[sample_index])
            power = ''
            if not self.added_features is None:
                power = self.added_features[sample_index]
            print("Signal power: ", power)
            df = pd.DataFrame(sample, columns=['x','y','z'])
            data_visualization.plot_dataframe(df, title=str(sample_index), label=label, text=str(power))

    def set_net(self, net:nn.Module):
        self.net = net


    def get_complete_dataset(self):
        if not self.net is None and not self.dataset is None:
            X = torch.Tensor(self.samples).view(-1, *self.get_tensor_sizes())
            y = torch.tensor(self.labels, dtype=torch.long)


            linear_feature = None

            if self.net.net_type.startswith("Hybrid"):
                if not self.added_features is None:
                    linear_feature = torch.Tensor(self.added_features)
                    linear_feature = linear_feature.numpy()
                else:
                    print("ERROR: using an hybrid CNN, but no linear features were found")
                    exit(1)
        else:
            print("ERROR: Net or dataset not initialized, use set_net() and/or load_dataset_from_files() ")
            exit(1)
        return X.numpy(), y.numpy(), linear_feature


    # For skorch (with LR scheduler) use only
    def set_optimizer(self):
        self.optimizer = optim.SGD(self.net.parameters())



if __name__ == '__main__':


    classifier = PotholeClassifier()
    classifier.load_dataset_from_files()

    # ConvNet 2D    Wavelet
    classifier.dataset_to_wavelet()
    classifier.zero_mean_norm()

    conv_net_2d = ConvNet(input_sizes=classifier.get_tensor_sizes(), dropout_p=0.5, nonlinear='relu', num_unit_per_conv_layer=DEFAULT_CONVS, num_unit_per_linear_layer=DEFAULT_LINEARS)
    print("---------------MODEL--------------------")
    print(conv_net_2d)
    classifier.set_net(conv_net_2d)
    classifier.train(conv_net_2d)
    acc, loss = classifier.test(test_set_X=classifier.training_set_X, test_set_y=classifier.training_set_y)
    val_acc, val_loss = classifier.test()
    print("Train Acc:", acc)
    print("Train Loss:", loss)
    print("Test Acc:", val_acc)
    print("Test Loss:", val_loss)
    classifier.confusion_matrix()
    # --------------------------------------------------


    # ConvNet 1D
    # classifier.zero_mean_norm()

    # conv_net_1d = ConvNet(input_sizes=classifier.get_tensor_sizes(),dropout_p=0.5, nonlinear='relu', num_unit_per_conv_layer=DEFAULT_CONVS, num_unit_per_linear_layer=DEFAULT_LINEARS)
    # print("---------------MODEL--------------------")
    # print(conv_net_1d)
    # classifier.set_net(conv_net_1d)
    # classifier.train(conv_net_1d)
    # acc, loss = classifier.test(test_set_X=classifier.training_set_X, test_set_y=classifier.training_set_y)
    # val_acc, val_loss = classifier.test()
    # print("Train Acc:", acc)
    # print("Train Loss:", loss)
    # print("Test Acc:", val_acc)
    # print("Test Loss:", val_loss)
    # classifier.confusion_matrix()

    # -----------------------------------------------------
